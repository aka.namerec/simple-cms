/*
This file is used to bootstrap development database.

Note: ONLY development database;
*/

CREATE USER simple_cms WITH PASSWORD 'simple_cms';
CREATE DATABASE simple_cms OWNER simple_cms ENCODING 'utf-8';
GRANT ALL ON DATABASE simple_cms TO simple_cms;
ALTER USER simple_cms createdb;
