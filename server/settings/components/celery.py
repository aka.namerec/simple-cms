from server.settings.components import config

CELERY_BROKER_URL = config("CELERY_BROKER_URL", default=None)
CELERY_TASK_ALWAYS_EAGER = not CELERY_BROKER_URL
