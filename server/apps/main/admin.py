# -*- coding: utf-8 -*-
from adminsortable2.admin import SortableInlineAdminMixin
from django.contrib import admin  # noqa: F401
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models import QuerySet
from gfklookupwidget.widgets import GfkLookupWidget

from . import page_contents
from .models import Page, PageContents, Audio, Video, Text


def available_content_types() -> QuerySet:
    result = None
    for model in page_contents.Registration.registration:
        q = models.Q(app_label=model._meta.app_label, model=model._meta.model_name)
        result = result | q if result else q
    qs = ContentType.objects.filter(result)
    return qs


class PageContentsInline(SortableInlineAdminMixin, admin.TabularInline):
    model = PageContents
    extra = 0

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'object_id':
            kwargs['widget'] = GfkLookupWidget(
                content_type_field_name='content_type',
                parent_field=PageContents._meta.get_field('content_type'),
            )
        field = super().formfield_for_dbfield(db_field, **kwargs)
        if db_field.name == 'content_type':
            field.queryset = available_content_types()
        return field


@admin.register(Page)
class PageAdmin(admin.ModelAdmin):
    list_display = ('id', 'title')
    search_fields = ('^title',)
    inlines = (PageContentsInline,)


@admin.register(Audio)
class AudioAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'audio_url', 'bitrate')
    list_display_links = ('title',)
    search_fields = ('^title',)


@admin.register(Video)
class VideoAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'video_url', 'subtitles_url')
    list_display_links = ('title',)
    search_fields = ('^title',)


@admin.register(Text)
class TextAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'text')
    list_display_links = ('title',)
    search_fields = ('^title',)
