import random

import mimesis
from django.core.management.base import BaseCommand
from django.db import transaction

from server.apps.main import models
from server.apps.main.management.commands.one_line_progress import OneLineProgress


class Command(BaseCommand):
    help = 'Generates the test data for Simple CMS.'

    def add_arguments(self, parser):
        parser.add_argument('--count', help='Count of pages to generate', type=int, default=1024)
        parser.add_argument('--max-page-contents', help='Maximum content items per page', type=int, default=10)
        parser.add_argument('--clean', help='Clean CMS tables before execution', type=bool, default=True)

    def handle(self, *args, **options):
        count = options['count']
        max_page_contents = options['max_page_contents']
        clean = options['clean']
        print(f'* Generating {count} pages.')
        print(f'* Generating maximum {max_page_contents} content items per page.')
        if clean:
            print('* CMS tables will be clean.')

        progress = OneLineProgress()
        with transaction.atomic():
            if clean:
                print('Cleaning CMS tables...')
                for model in [models.Video, models.Audio, models.Text, models.PageContents, models.Page]:
                    model.objects.all().delete()

            print(f'Creating {count} Pages...')
            for index in range(0, count):
                page = models.Page.objects.create(title=generate_title())
                for i in range(random.randrange(1, max_page_contents)):
                    page.contents.create(order=i + 1, content_object=random_content_object())
                progress.update(
                    lambda: f'Item {index} of {count} - {(index / count) * 100:.2f}%.',
                    index == count - 1
                )
        progress.clear()
        print('Done.')


def generate_title() -> str:
    return mimesis.Text().title()[:models._TITLE_MAX_LENGTH]


def generate_video() -> models.Video:
    url: str = mimesis.Internet().home_page()
    return models.Video.objects.create(
        title=generate_title(),
        video_url=url,
        subtitles_url=f'{url}/subtitles'
    )


def generate_audio() -> models.Video:
    return models.Audio.objects.create(
        title=generate_title(),
        audio_url=mimesis.Internet().home_page(),
        bitrate=random.choice([96, 128, 160, 190, 320])
    )


def generate_text() -> models.Video:
    return models.Text.objects.create(
        title=generate_title(),
        text=mimesis.Text().text(random.randrange(5, 21))
    )


def random_content_object() -> models.PageContentsMixin:
    return random.choice([generate_video, generate_audio, generate_text])()
