from math import trunc
from time import time
from typing import Callable


class OneLineProgress:
    def __init__(self):
        self.len = 0
        self.t = 0
        self.assign_time()

    def assign_time(self):
        self.t = get_time_sec()

    def clear(self):
        print('\r' + ' ' * self.len + '\r', end='')

    def update(self, cb: Callable[[], str], force: bool = False) -> None:
        if not force and not (get_time_sec() > self.t):
            return
        message: str = cb()
        message_len = len(message)
        if message_len < self.len:
            self.clear()
        print(f'\r{message}', end='')
        self.len = message_len
        self.assign_time()


def get_time_sec() -> int:
    return trunc(time())
