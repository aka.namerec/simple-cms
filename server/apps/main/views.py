# -*- coding: utf-8 -*-

from django.http import HttpRequest, HttpResponse
from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.generics import get_object_or_404
from rest_framework.mixins import ListModelMixin
from rest_framework.response import Response

from server.apps.main.models import Page
from server.apps.main.serializers import PageListSerializer, PageDetailsSerializer
from server.apps.main.tasks import update_page_visit_counter


def index(request: HttpRequest) -> HttpResponse:
    """
    Main (or index) view.

    Returns rendered default page to the user.
    Typed with the help of ``django-stubs`` project.
    """
    return render(request, 'main/index.html')


class PagesViewSet(ListModelMixin, viewsets.GenericViewSet):
    queryset = Page.objects.all()
    serializer_class = PageListSerializer

    def retrieve(self, request, pk=None):
        queryset = Page.objects.all()
        page = get_object_or_404(queryset, pk=pk)
        serializer = PageDetailsSerializer(page)
        data = serializer.data
        update_page_visit_counter.delay(pk)
        return Response(data)
