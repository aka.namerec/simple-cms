from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from server.apps.main import page_contents
from server.apps.main.models import Page, PageContents, Text, Video, Audio
from server.apps.main.page_contents import Registration


class PageListSerializer(ModelSerializer):
    details = serializers.HyperlinkedIdentityField(
        view_name='pages-detail',
        lookup_field='pk',
    )

    class Meta:
        model = Page
        fields = ('title', 'created_at', 'modified_at', 'details')


class ContentObjectRelatedField(serializers.RelatedField):
    def to_representation(self, value):
        serializer_class = page_contents.Registration.registration.get(value.__class__)
        return serializer_class(value).data if serializer_class else f'Unknown content type "{value.__class__}".'


class PageContentsSerializer(ModelSerializer):
    content_object = ContentObjectRelatedField(read_only=True)

    class Meta:
        model = PageContents
        fields = ('order', 'content_object')


class PageDetailsSerializer(ModelSerializer):
    contents = PageContentsSerializer(many=True, read_only=True)

    class Meta:
        model = Page
        fields = ('id', 'title', 'created_at', 'modified_at', 'contents')


class AudioSerializer(ModelSerializer):
    class Meta:
        model = Audio
        fields = '__all__'


class TextSerializer(ModelSerializer):
    class Meta:
        model = Text
        fields = '__all__'


class VideoSerializer(ModelSerializer):
    class Meta:
        model = Video
        fields = '__all__'


Registration.register(AudioSerializer)
Registration.register(VideoSerializer)
Registration.register(TextSerializer)
