# -*- coding: utf-8 -*-

import textwrap

from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models, transaction
from gfklookupwidget.fields import GfkLookupField
from typing_extensions import Final, final

#: That's how constants should be defined.

_TITLE_MAX_LENGTH: Final = 128
_URL_MAX_LENGTH: Final = 256


class TimeStampedMixin(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


@final
class Page(TimeStampedMixin):
    title = models.CharField(max_length=_TITLE_MAX_LENGTH)

    def __str__(self) -> str:
        return textwrap.wrap(self.title, _TITLE_MAX_LENGTH // 4)[0]

    @staticmethod
    def consider_visit(page_id: int) -> None:
        qs_page_contents = PageContents.objects.select_for_update().filter(page_id=page_id)
        with transaction.atomic():
            for contents_item in qs_page_contents:
                content_object = contents_item.content_object
                if not isinstance(content_object, PageContentsMixin):
                    continue
                content_object.visits_counter += 1
                content_object.save()


@final
class PageContents(TimeStampedMixin):
    page = models.ForeignKey(to=Page, on_delete=models.CASCADE, related_name='contents', null=False, default=None)
    order = models.PositiveIntegerField(default=0, blank=False, null=False)
    content_type = models.ForeignKey(ContentType, on_delete=models.PROTECT, null=False, default=None)
    object_id = GfkLookupField('content_type')
    content_object = GenericForeignKey('content_type', 'object_id')

    class Meta:
        ordering = ('order',)

    def __str__(self) -> str:
        return str(self.content_object) if self.content_object else ''


class PageContentsMixin(TimeStampedMixin):
    title = models.CharField(max_length=_TITLE_MAX_LENGTH)
    visits_counter = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta:
        abstract = True


@final
class Video(PageContentsMixin):
    video_url = models.URLField(max_length=_URL_MAX_LENGTH)
    subtitles_url = models.URLField(max_length=_URL_MAX_LENGTH, blank=True, null=True)

    def __str__(self) -> str:
        return (
            f'"{self.title}": {self.video_url}' +
            (f'with subtitles {self.subtitles_url}' if self.subtitles_url else '')
        )


@final
class Audio(PageContentsMixin):
    audio_url = models.URLField(max_length=_URL_MAX_LENGTH)
    bitrate = models.PositiveIntegerField(default=0, blank=True, null=True)

    def __str__(self) -> str:
        return f'"{self.title}": {self.audio_url}' + f'bitrate: {self.bitrate}' if self.bitrate else ''


@final
class Text(PageContentsMixin):
    text = models.TextField()

    def __str__(self) -> str:
        return f'"{self.title}": {textwrap.wrap(self.text, _TITLE_MAX_LENGTH)[0]}'
