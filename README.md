# simple_cms

Simple CMS

This project was generated with [`wemake-django-template`](https://github.com/wemake-services/wemake-django-template). Current template version is: [cb418381190b9fd642b0f2215c7362e9c7828d94](https://github.com/wemake-services/wemake-django-template/tree/cb418381190b9fd642b0f2215c7362e9c7828d94). See what is [updated](https://github.com/wemake-services/wemake-django-template/compare/cb418381190b9fd642b0f2215c7362e9c7828d94...master) since then.

[![wemake.services](https://img.shields.io/badge/%20-wemake.services-green.svg?label=%20&logo=data%3Aimage%2Fpng%3Bbase64%2CiVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAABGdBTUEAALGPC%2FxhBQAAAAFzUkdCAK7OHOkAAAAbUExURQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP%2F%2F%2F5TvxDIAAAAIdFJOUwAjRA8xXANAL%2Bv0SAAAADNJREFUGNNjYCAIOJjRBdBFWMkVQeGzcHAwksJnAPPZGOGAASzPzAEHEGVsLExQwE7YswCb7AFZSF3bbAAAAABJRU5ErkJggg%3D%3D)](https://wemake.services) [![build status](https://gitlab.com/aka.namerec/simple_cms/badges/master/build.svg)](https://gitlab.com/aka.namerec/simple_cms/commits/master) [![coverage report](https://gitlab.com/aka.namerec/simple_cms/badges/master/coverage.svg)](https://gitlab.com/aka.namerec/simple_cms/commits/master) [![wemake-python-styleguide](https://img.shields.io/badge/style-wemake-000000.svg)](https://github.com/wemake-services/wemake-python-styleguide)

Full documentation for this template can be found in the `docs` directory.

## Prerequisites

`docker` with [version at least](https://docs.docker.com/compose/compose-file/#compose-and-docker-compatibility-matrix) `18.02`

## Technological stack

* docker & docker-compose
* Python 3.8, Django 2.2 with REST framework (DRF)
* PostgreSQL
* Celery
* Redis

## How to run

### Server application

1.  Clone this repository in empty folder.

2.  Please, put the following text in file `.env` in config folder:
    ```.env
    # Security Warning! Do not commit this file to any VCS!
    # This is a local file to speed up development process,
    # so you don't have to change your environment variables.
    #
    # This is not applied to `.env.template`!
    # Template files must be committed to the VCS, but must not contain
    # any secret values.
    
    
    # === General ===
    
    DOMAIN_NAME=gitlab.com/aka.namerec
    TLS_EMAIL=webmaster@gitlab.com/aka.namerec
    
    
    # === Django ===
    # Generate yours with:
    # python3 -c 'import secrets; print(secrets.token_hex(50))'
    
    DJANGO_SECRET_KEY=UOp?~s[|7B`{u?syRn!o&Gy+XL!sHqb7>|F8G9?D=#/VGsL)i(
    
    
    # === Database ===
    
    # These variables are special, since they are consumed
    # by both django and postgres docker image.
    # Cannot be renamed if you use postgres in docker.
    # See: https://hub.docker.com/_/postgres
    
    POSTGRES_DB=simple_cms
    POSTGRES_USER=simple_cms
    POSTGRES_PASSWORD=simple_cms
    
    # Used only by django:
    DJANGO_DATABASE_HOST=localhost
    DJANGO_DATABASE_PORT=5432
    
    # === Celery ====
    
    CELERY_BROKER_URL=redis://cache
    ```
    I could have put it there myself, but this is wrong, since `.env` may contain sensitive information, and *should never* be stored in source code repository.

3. Run `docker-compose build`.

4. Run `docker-compose up`.

On first start will executed all need migrations, include data migration, which enters example data in a DB.
   
After this steps DB will contain example data, containers with server application and need infrastructure (DB, Celery and Redis) will started.

After this application will be available by address `0.0.0.0:8000`.

Admin login is `admin`, password `2048`.

### Autotests

1. Go to the directory of this project.
2. Run `autotests` script, which start docker container for tests execution.

## Tips

### Admin Panel

In admin panel of Pages you can change the order of content items by drag&drop.

You can also select the desired content item from the corresponding table.

See image below:

![](data/20200315-084923.png)

# Endpoints

You can see how endpoints work if you follow the link from the main page of the site. Uses the standard DRF interface.

![](data/20200315-085841.png)

## Q&A

### Where my source codes?

1. Basically in `server/apps/main` folder.
2. See also `server/settings` - updated configuration from *wemake team* for this project.
3. Tests for two endpoints (required by task specifications) - in `tests/test_apps/test_main/test_api/test_pages.py`.

### How to add new Page Content type?

Very simple.

1. Create you own *Content Type* model inheriting it from `server.apps.main.models.PageContentsMixin`.

   See examples in `server/apps/main/models.py` - `Audio`, `Video` and `Text` *Content Types* "from the box".

2. Write serializer of you model for second endpoint (which return the details of the Page).

   See examples in `server/apps/main/serializers.py`, which contains serializers for existent *Content Types*.

3. And register you serializer by `server.apps.main.page_contents.Registration.register` static method.
   
   See examples on the end of `server/apps/main/serializers.py` module. 
